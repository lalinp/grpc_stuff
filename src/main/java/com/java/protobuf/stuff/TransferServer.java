package com.java.protobuf.stuff;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;

public class TransferServer {

  public static void main(String[] args) throws IOException, InterruptedException {

    Server server = ServerBuilder
        .forPort(8080)
        .addService(new TransferServiceImpl()) // this is how you do DI - grpc stylie!
        .build();

    server.start();
    server.awaitTermination();
  }
}
