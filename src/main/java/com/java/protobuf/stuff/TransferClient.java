package com.java.protobuf.stuff;

import com.java.protobuff.stuff.BankGrpc;
import com.java.protobuff.stuff.BankGrpc.BankBlockingStub;
import com.java.protobuff.stuff.Transfer;
import com.java.protobuff.stuff.TransferResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class TransferClient {
  public static void main(String[] args) {

      //first we need a channel this defines the port and the host
    ManagedChannel managedChannel = ManagedChannelBuilder
        .forAddress("localhost",8080)
        .usePlaintext() //http2 comes with https by default and on my local machine I dont want to use https (use http)
        .build();

    /**
     * everything in grpc is async
     * the client can choose be a non-blocking client or
     * a blocking client (stub so client == stub)
     * this is done by creating a cleint stub
     * choosing a blocking client option for now
     */

    BankBlockingStub client = BankGrpc.newBlockingStub(managedChannel);

    Transfer transfer = Transfer.newBuilder()
        .setAccountFrom(123)
        .setAccountTo(456)
        .setAmount(10.00)
        .setId(10)
        .setMessage("here is your money")
        .build();

    TransferResponse response = client.transfer(transfer);

    System.out.println("response " + response);
  }
}
