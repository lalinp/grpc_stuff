package com.java.protobuf.stuff;

import com.java.protobuff.stuff.BankGrpc;
import com.java.protobuff.stuff.Transfer;
import com.java.protobuff.stuff.TransferResponse;
import io.grpc.stub.StreamObserver;

public class TransferServiceImpl extends BankGrpc.BankImplBase {

  @Override
  public void transfer(Transfer request,
      StreamObserver<TransferResponse> responseObserver){

      responseObserver.onNext(TransferResponse.newBuilder()
          .setId(request.getId())
          .setMessage("all is well - thanks for the money")
          .build());
      //responseObserver.onError(new Throwable("something happened"));
      responseObserver.onCompleted();
  }

}
